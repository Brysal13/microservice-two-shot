# Wardrobify

Team:

* Bryant Salazar - Hats Microservice
* Person 2 - Which microservice?

## Design
The Wardrobify microservice is meant to be scalable, maintable, and secure. This was built using the Django REST framework, which makes it so building with RESTful APIs can easily handle HTTP requests and responses.
## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice

Hats Api
- Views
- Get, Post, Delete
- list encoder, , detail encoder, location encoder
Poller
- Makes an API call using requests to Wardrobe API.
- makes LocationVO objects based on Location id
React Hats Route
- Create, Delete, and List
