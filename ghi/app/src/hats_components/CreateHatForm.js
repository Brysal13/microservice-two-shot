import { useState, useEffect } from "react"

const CreateHatForm = ({locations, fetchHatData}) => {

  const [fabric , setFabric ] = useState("")
  const [style , setStyle ] = useState("")
  const [color , setColor ] = useState("")
  const [picture , setPicture ] = useState("")
  const [location , setLocation ] = useState("")

  const UpdateForm = ({target}, callBack) => {
    const {value} = target;
    callBack(value);
  }

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.fabric = fabric;
    data.style_name = style;
    data.color = color;
    data.picture_url = picture;
    data.location = location;
    console.log(data)

    const url = "http://localhost:8090/api/hats/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
          "Content-Type": "application/json",
      }
    }
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      const responseData = await response.json();

      fetchHatData()

      setFabric("")
      setStyle("")
      setColor("")
      setPicture("")
      setLocation("")
      }
  }

  return (
    <div className="modal fade" id="createhat" tabindex="-1" aria-labelledby="createhatLabel" aria-hidden="true">
      <div className="modal-dialog">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="createhatLabel">Create a new hat</h5>
            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div className="modal-body">
            <form onSubmit={handleSubmit}>
              <div className="mb-3">
                <label htmlFor="fabric" className="col-form-label">Fabric</label>
                <input
                  value={fabric}
                  onChange={(event) => UpdateForm(event, setFabric)}
                  type="text"
                  className="form-control"
                  id="fabric" />
              </div>
              <div className="mb-3">
                <label htmlFor="stylename" className="col-form-label">Style Name</label>
                <input
                  value={style}
                  onChange={(event) => UpdateForm(event, setStyle)}
                  type="text"
                  className="form-control"
                  id="stylename" />
              </div>
              <div className="mb-3">
                <label htmlFor="color" className="col-form-label">Color</label>
                <input
                  value={color}
                  onChange={(event) => UpdateForm(event, setColor)}
                  type="text"
                  className="form-control"
                  id="color" />
              </div>
              <div className="mb-3">
                <label htmlFor="pictureurl" className="col-form-label">Picture url</label>
                <input
                  value={picture}
                  onChange={(event) => UpdateForm(event, setPicture)}
                  className="form-control"
                  id="pictureurl"></input>
              </div>
              <div className="mb-3">
                <select value={location} onChange={(event) => UpdateForm(event, setLocation)} className="form-select" aria-label="Default select example">
                  <option value="">Open this select menu</option>
                  {locations.map(location => {
                    return (
                      <option value={location.id} key={location.href}>{location.closet_name}</option>
                    )
                  })}
                </select>
              </div>
              <div className="modal-footer">
            <button className="btn btn-outline-dark" data-bs-dismiss="modal">submit</button>
          </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  )
}

export default CreateHatForm;
