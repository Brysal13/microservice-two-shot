const Delete = ({id, fetchHatData}) => {

    const thisId = `mymodel${id}`
    const reference = `#mymodel${id}`

    const handleSubmit = async (event) => {
      event.preventDefault()

      const url = `http://localhost:8090/api/hats/${id}/`
      const fetchConfig = {
        method: "delete",
      }
      const response = await fetch(url, fetchConfig)
      if (response.ok) {
        fetchHatData()
      }
      console.log(id)
      console.log(thisId)
    }
    return (
      <>
        <button type="button" className="btn btn-sm btn-danger" data-bs-toggle="modal" data-bs-target={reference}>
          Delete
        </button>

        <div className="modal fade" id={thisId} tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">Delete Hat</h5>
                <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div className="modal-body">
              <p>Are you sure you want to Delete this hat?</p>
              </div>
              <div className="modal-footer">
              <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
              <button onClick={handleSubmit} className="btn btn-danger" data-bs-dismiss="modal">Delete</button>
              </div>
            </div>
          </div>
        </div>
      </>
    )
  }


  export default Delete;
