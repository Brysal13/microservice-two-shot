import {useState, useEffect} from "react";
import HatCard from "./HatCard";
import CreateHatForm from "./CreateHatForm";


const Hats = ({hats, locations, fetchHatData}) => {

    return (
    <>
    <br />
      <div className="container" >
        <div className="row">
              <h1 className="col m-0">Hats</h1>
              <button type="button" className="w-25 btn btn-outline-dark" data-bs-toggle="modal" data-bs-target="#createhat" data-bs-whatever="@mdo">Create a Hat</button>
        </div>
      </div>
    <br />
      <CreateHatForm fetchHatData={fetchHatData} hats={hats} locations={locations} />

      <div className="row gy-3">
        <HatCard hats={hats} fetchHatData={fetchHatData} />
      </div>
    </>
    )
  }


export default Hats;
