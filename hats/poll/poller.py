import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()

# Import models from hats_rest, here.
from hats_rest.models import LocationVO, Hat
# from hats_rest.models import Something

WARDROBE_LOCATION_API = "http://wardrobe-api:8000/api/locations/"

def poll():
    while True:
        print("getting Polling Data")

        try:
            response = requests.get(WARDROBE_LOCATION_API)
            data = json.loads(response.content)
            locations = data["locations"]

            for location in locations:
                LocationVO.objects.update_or_create(
                    import_href = location["href"],
                    closet_name = location["closet_name"],
                    section_number = location["section_number"],
	                shelf_number = location["shelf_number"]
                )

        except Exception as e:
            print(e, file=sys.stderr)
            print("failed try block")
        time.sleep(60)


if __name__ == "__main__":
    poll()
